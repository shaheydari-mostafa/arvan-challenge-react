import { defaultHeaders } from 'utils/request';
import {
  getUserLoggedIn,
  getUserLoggedOut,
} from '../containers/Dashboard/actions';

const tokenMiddleware = () => next => action => {
  if (action.type === getUserLoggedIn()) {
    window.localStorage.setItem('jwt_token', action.user.token);
    defaultHeaders.Authorization = `Token ${action.user.token}`;
  }
  if (action.type === getUserLoggedOut()) {
    delete defaultHeaders.Authorization;
    window.localStorage.removeItem('jwt_token');
  }

  next(action);
};
export default tokenMiddleware;
