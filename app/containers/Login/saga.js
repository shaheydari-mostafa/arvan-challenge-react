import { takeLatest, put, call } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import { userLoggedIn } from 'containers/Dashboard/actions';
import request from 'utils/request';
import { BASE_URL } from 'containers/App/constants';
import { showNotification } from 'containers/Notification/actions';
import { getStartLogin, startLoading, stopLoading, hasError } from './actions';

const LOGIN_URL = `${BASE_URL}/api/users/login`;
export function* login(action) {
  const { email, password } = action;
  const body = JSON.stringify({
    user: {
      email,
      password,
    },
  });

  yield put(startLoading());

  try {
    const result = yield call(request, LOGIN_URL, {
      body,
      method: 'POST',
    });
    yield put(userLoggedIn(result.user));
    yield put(stopLoading());
    yield put(push('/articles'));
  } catch (err) {
    yield put(stopLoading());
    yield put(showNotification('danger', err));
    yield put(hasError());
  }
}

export default function* loginApis() {
  yield takeLatest(getStartLogin(), login);
}
