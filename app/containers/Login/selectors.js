import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectLogin = state => state.login || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectLogin,
    loginState => loginState.isLoading,
  );

const makeSelectError = () =>
  createSelector(
    selectLogin,
    loginState => loginState.hasError,
  );

export { makeSelectLoading, makeSelectError, selectLogin };
