export const getStartLogin = () => 'LOGIN_PAGE/START_LOGIN';
export const getStartLoding = () => 'LOGIN_PAGE/START_LOADING';
export const getStopLoding = () => 'LOGIN_PAGE/STOP_LOADING';
export const getHasError = () => 'LOGIN_PAGE/HAS_ERROR';

export const startLogin = (email, password) => ({
  type: getStartLogin(),
  password,
  email,
});
export const startLoading = () => ({ type: getStartLoding() });
export const stopLoading = () => ({ type: getStopLoding() });
export const hasError = () => ({ type: getHasError() });
