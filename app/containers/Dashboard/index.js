import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { useInjectReducer } from 'utils/injectReducer';
import TopNav from 'components/TopNav';
import LeftSideNav from 'components/LeftSideNav';
import ArticlesList from 'containers/ArticlesList';
import ArticleCreate from 'containers/ArticleCreate';
import ArticleEdit from 'containers/ArticleEdit';
import reducer from './reducer';
import { makeSelectUser } from './selectors';

const ColWithColor = styled(Col)`
  background-color: ${props => props.bcolor};
`;
const ColWithMinHeight = styled(ColWithColor)`
  min-height: calc(100vh - 60px);
`;
const key = 'dashboard';

export function Dashboard(props) {
  useInjectReducer({ key, reducer });
  return (
    <React.Fragment>
      <Row>
        <ColWithColor bcolor="#343a40" xs="12">
          <TopNav username={props.user ? props.user.username : ''} />
        </ColWithColor>
      </Row>
      <Row>
        <ColWithMinHeight className="d-none d-md-block" md="2" bcolor="#1c7cd5">
          <LeftSideNav />
        </ColWithMinHeight>
        <Col md="10">
          <Switch>
            <Route
              exact
              path={['/articles', '/articles/page/:page']}
              component={ArticlesList}
            />
            <Route exact path="/articles/create" component={ArticleCreate} />
            <Route exact path="/articles/edit/:slug" component={ArticleEdit} />
          </Switch>
        </Col>
      </Row>
    </React.Fragment>
  );
}

Dashboard.propTypes = {
  user: PropTypes.object,
};
const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
});

const withConnect = connect(mapStateToProps);

export default compose(withConnect)(Dashboard);
