import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectDashboard = state => state.dashboard || initialState;

const makeSelectUser = () =>
  createSelector(
    selectDashboard,
    dashboardState => dashboardState.user,
  );
const makeSelectTags = () =>
  createSelector(
    selectDashboard,
    dashboardState => dashboardState.tags,
  );

export { makeSelectUser, selectDashboard, makeSelectTags };
