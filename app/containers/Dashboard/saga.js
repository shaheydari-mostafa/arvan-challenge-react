import { call, put } from 'redux-saga/effects';
import { BASE_URL } from 'containers/App/constants';
import request from 'utils/request';

import { tagsFetched } from './actions';

const GET_TAGS_URL = `${BASE_URL}/api/tags`;
export function* get() {
  const result = yield call(request, GET_TAGS_URL);
  yield put(tagsFetched(result.tags));
}
