import sort from 'utils/sort';

import { getUserLoggedIn, getTagsFetched, getAddNewTag } from './actions';

export const initialState = {};

const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case getUserLoggedIn():
      return { ...state, user: action.user };
    case getTagsFetched():
      return { ...state, tags: action.tags };
    case getAddNewTag():
      return { ...state, tags: sort([...state.tags, action.tag]) };
    default:
      return state;
  }
};
export default dashboardReducer;
