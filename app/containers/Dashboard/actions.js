export const getUserLoggedIn = () => 'DASHBOARD_USER_LOGGED_IN';
export const getUserLoggedOut = () => 'DASHBOARD_USER_LOGGED_OUT';
export const getGetTags = () => 'DASHBOARD/GET_TAGS';
export const getTagsFetched = () => 'DASHBOARD/TAGS_FETCHED';
export const getAddNewTag = () => 'DASHBOARD/ADD_NEW_TAG';

export const addNewTag = tag => ({ type: getAddNewTag(), tag });
export const getTags = () => ({ type: getGetTags() });
export const tagsFetched = tags => ({ type: getTagsFetched(), tags });
export const userLoggedIn = user => ({ type: getUserLoggedIn(), user });
export const userLoggedOut = () => ({ type: getUserLoggedOut() });
