export const getStartRegister = () => 'REGISTER_PAGE/START_REGISTER';
export const getStartLoding = () => 'REGISTER_PAGE/START_LOADING';
export const getStopLoding = () => 'REGISTER_PAGE/STOP_LOADING';
export const getHasError = () => 'REGISTER_PAGE/HAS_ERROR';

export const startRegister = (email, password, user) => ({
  type: getStartRegister(),
  password,
  email,
  user,
});
export const startLoading = () => ({ type: getStartLoding() });
export const stopLoading = () => ({ type: getStopLoding() });
export const hasError = () => ({ type: getHasError() });
