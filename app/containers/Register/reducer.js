import { getStartLoding, getStopLoding } from './actions';

export const initialState = {
  isLoading: false,
};
const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case getStartLoding():
      return { ...state, isLoading: true };
    case getStopLoding():
      return { ...state, isLoading: false };
    default:
      return state;
  }
};
export default registerReducer;
