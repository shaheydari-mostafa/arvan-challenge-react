import { takeLatest, put, call } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import request from 'utils/request';
import { BASE_URL } from 'containers/App/constants';
import { userLoggedIn } from 'containers/Dashboard/actions';
import { showNotification } from 'containers/Notification/actions';
import {
  getStartRegister,
  startLoading,
  stopLoading,
  hasError,
} from './actions';

const REGISTER_URL = `${BASE_URL}/api/users`;
export function* register(action) {
  const { user, email, password } = action;
  const body = JSON.stringify({
    user: {
      username: user,
      email,
      password,
    },
  });

  yield put(startLoading());

  try {
    const result = yield call(request, REGISTER_URL, {
      body,
      method: 'POST',
    });
    yield put(userLoggedIn(result.user));
    yield put(stopLoading());
    yield put(push('/articles'));
  } catch (err) {
    yield put(stopLoading());
    yield put(showNotification('danger', err));
    yield put(hasError());
  }
}

export default function* registerApis() {
  yield takeLatest(getStartRegister(), register);
}
