/**
 * RegisterPage
 */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import RegisterForm from 'components/RegisterForm';
import CustomInput from 'components/CustomInput';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import { makeSelectLoading, makeSelectError } from './selectors';
import { startRegister } from './actions';

const key = 'register';

export function Register({ onSubmitForm, loading }) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [user, setUser] = useState('');
  const [validate, setValidate] = useState({
    emailValid: true,
    passwordValid: true,
    userValid: true,
  });
  const handleInput = ev => {
    if (!ev) {
      return;
    }
    if (ev.target.name === 'email') {
      setEmail(ev.target.value);
    }
    if (ev.target.name === 'password') {
      setPassword(ev.target.value);
    }
    if (ev.target.name === 'user') {
      setUser(ev.target.value);
    }
  };
  const submitForm = evt => {
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    setValidate({
      emailValid: !!email,
      passwordValid: !!password,
      userValid: !!user,
    });
    if (!!email && !!password && !!user) {
      onSubmitForm(email, password, user);
    }
  };
  return (
    <RegisterForm onSubmitForm={submitForm}>
      <CustomInput
        type="text"
        name="user"
        id="userField"
        label="User"
        value={user}
        valid={validate.userValid}
        onChange={handleInput}
      />
      <CustomInput
        type="email"
        name="email"
        id="emailField"
        label="Email"
        value={email}
        valid={validate.emailValid}
        onChange={handleInput}
      />
      <CustomInput
        type="password"
        name="password"
        id="passwordField"
        label="Password"
        value={password}
        valid={validate.passwordValid}
        onChange={handleInput}
      />
      <Button color="primary" block disabled={loading}>
        Register
      </Button>
    </RegisterForm>
  );
}

Register.propTypes = {
  onSubmitForm: PropTypes.func,
  loading: PropTypes.bool,
};
const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onSubmitForm: (email, password, user) => {
      dispatch(startRegister(email, password, user));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Register);
