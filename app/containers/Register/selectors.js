import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectRegister = state => state.register || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectRegister,
    registerState => registerState.isLoading,
  );

const makeSelectError = () =>
  createSelector(
    selectRegister,
    registerState => registerState.hasError,
  );

export { makeSelectLoading, makeSelectError, selectRegister };
