/**
 * App.js
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import styled from 'styled-components';
import { Container } from 'reactstrap';

import HomePage from 'containers/HomePage';
import NotFoundPage from 'containers/NotFoundPage';
import Login from 'containers/Login';
import Register from 'containers/Register';
import Dashboard from 'containers/Dashboard';
import Notification from 'containers/Notification';
import Logout from 'containers/Logout';

import GlobalStyle from '../../global-styles';
const Wrapper = styled.div`
  width: 100%;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  min-height: 100%;
  height: 100%;
  flex-direction: column;
`;
export default function App() {
  return (
    <Wrapper>
      <Container fluid className="h-100">
        <Notification />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route path="/articles" component={Dashboard} />
          <Route path="/logout" component={Logout} />
          <Route component={NotFoundPage} />
        </Switch>
        <GlobalStyle />
      </Container>
    </Wrapper>
  );
}
