import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectArticleNew = state => state.article_create || initialState;

const makeSelectArticle = () =>
  createSelector(
    selectArticleNew,
    articleSate => articleSate.article,
  );

const makeSelectLoading = () =>
  createSelector(
    selectArticleNew,
    articleState => articleState.isLoading,
  );

const makeSelectError = () =>
  createSelector(
    selectArticleNew,
    articleState => articleState.hasError,
  );

const makeSelectSelectedTags = () =>
  createSelector(
    selectArticleNew,
    articleState => articleState.selectedTags,
  );
export {
  makeSelectArticle,
  selectArticleNew,
  makeSelectLoading,
  makeSelectError,
  makeSelectSelectedTags,
};
