import { takeLatest, put, call, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import { BASE_URL } from 'containers/App/constants';
import request from 'utils/request';
import { get } from 'containers/Dashboard/saga';
import { getGetTags } from 'containers/Dashboard/actions';
import { showNotification } from 'containers/Notification/actions';
import {
  getSubmitForm,
  startLoading,
  stopLoading,
  hasError,
  saved,
} from './actions';
import { makeSelectArticle, makeSelectSelectedTags } from './selectors';

const ARTICLE_CREATE_URL = `${BASE_URL}/api/articles`;
export function* create() {
  const article = yield select(makeSelectArticle());
  article.tagList = yield select(makeSelectSelectedTags());
  const body = JSON.stringify({
    article,
  });
  yield put(startLoading());
  try {
    // eslint-disable-next-line no-unused-vars
    const result = yield call(request, ARTICLE_CREATE_URL, {
      body,
      method: 'POST',
    });
    yield put(
      showNotification('success', 'Well done! Article created successfuly'),
    );
    yield put(stopLoading());
    yield put(saved());
    yield put(push('/articles'));
  } catch (err) {
    yield put(stopLoading());
    yield put(showNotification('danger', err));
    yield put(hasError());
  }
}

export default function* createApis() {
  yield takeLatest(getSubmitForm(), create);
  yield takeLatest(getGetTags(), get);
}
