import {
  getChangeBody,
  getChangeTitle,
  getChangeDescription,
  getSaved,
  getStartLoding,
  getStopLoding,
  getSelectTag,
  getUnSelectTag,
} from './actions';

export const initialState = {
  isLoading: false,
  article: {
    title: '',
    description: '',
    body: '',
  },
  selectedTags: [],
};

const articleCreateReducer = (state = initialState, action) => {
  switch (action.type) {
    case getChangeBody():
      return {
        ...state,
        article: {
          ...state.article,
          body: action.body,
        },
      };
    case getChangeTitle():
      return {
        ...state,
        article: {
          ...state.article,
          title: action.title,
        },
      };
    case getChangeDescription():
      return {
        ...state,
        article: {
          ...state.article,
          description: action.description,
        },
      };
    case getStartLoding():
      return { ...state, isLoading: true };
    case getStopLoding():
      return { ...state, isLoading: false };
    case getSaved():
      return initialState;
    case getSelectTag():
      return { ...state, selectedTags: [...state.selectedTags, action.tag] };
    case getUnSelectTag():
      return {
        ...state,
        selectedTags: state.selectedTags.filter(tag => tag !== action.tag),
      };
    default:
      return state;
  }
};

export default articleCreateReducer;
