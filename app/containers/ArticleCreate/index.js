import React, { useState, useEffect } from 'react';
import { Form, Button, Col, Row } from 'reactstrap';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import DashboardPage from 'components/DashboardPage';
import CustomInput from 'components/CustomInput';
import TagSection from 'components/TagSection';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import { getTags, addNewTag } from 'containers/Dashboard/actions';
import { makeSelectTags } from 'containers/Dashboard/selectors';
import reducer from './reducer';
import saga from './saga';
import {
  changeBody,
  changeDescription,
  changeTitle,
  submitForm,
  unSelectTag,
  selectTag,
} from './actions';
import {
  makeSelectArticle,
  makeSelectLoading,
  makeSelectSelectedTags,
} from './selectors';

const key = 'article_create';
export function ArticleCreate({
  onSubmitForm,
  handleInput,
  article,
  loading,
  getTagsService,
  tags,
  selectedTags,
  onSelectTag,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  useEffect(() => {
    getTagsService();
  }, []);
  const [validate, setValidate] = useState({
    titleValid: true,
    descriptionValid: true,
    bodyValid: true,
  });
  const beforSubmit = evt => {
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    setValidate({
      titleValid: !!article.title,
      descriptionValid: !!article.description,
      bodyValid: !!article.body,
    });
    if (!!article.title && !!article.description && !!article.body) {
      onSubmitForm();
    }
  };
  return (
    <DashboardPage pageTitle="New Article">
      <Row>
        <Col xs="12" md="6">
          <Form onSubmit={beforSubmit}>
            <CustomInput
              name="title"
              type="text"
              id="titleField"
              label="Title"
              valid={validate.titleValid}
              onChange={handleInput}
              value={article.title}
            />
            <CustomInput
              name="description"
              type="text"
              id="descriptionField"
              label="Description"
              valid={validate.descriptionValid}
              onChange={handleInput}
              value={article.description}
            />
            <CustomInput
              name="body"
              type="textarea"
              id="bodyField"
              label="Body"
              valid={validate.bodyValid}
              onChange={handleInput}
              value={article.body}
            />
            <Button color="primary" disabled={loading}>
              Submit
            </Button>
          </Form>
        </Col>
        <Col xs="12" md="3">
          <TagSection
            tags={tags}
            selectedTags={selectedTags}
            onSelectTag={onSelectTag}
          />
        </Col>
      </Row>
    </DashboardPage>
  );
}

ArticleCreate.propTypes = {
  onSubmitForm: PropTypes.func,
  loading: PropTypes.bool,
  handleInput: PropTypes.func,
  article: PropTypes.shape({
    body: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
  }),
  getTagsService: PropTypes.func,
  tags: PropTypes.array,
  selectedTags: PropTypes.array,
  onSelectTag: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  article: makeSelectArticle(),
  loading: makeSelectLoading(),
  tags: makeSelectTags(),
  selectedTags: makeSelectSelectedTags(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onSubmitForm: () => {
      dispatch(submitForm());
    },
    handleInput: ev => {
      if (ev.target.name === 'body') {
        dispatch(changeBody(ev.target.value));
      } else if (ev.target.name === 'description') {
        dispatch(changeDescription(ev.target.value));
      } else if (ev.target.name === 'title') {
        dispatch(changeTitle(ev.target.value));
      }
    },
    getTagsService: () => {
      dispatch(getTags());
    },
    onSelectTag: (tag, select, newTag = false) => {
      if (newTag) {
        dispatch(addNewTag(tag));
      }
      dispatch(select ? selectTag(tag) : unSelectTag(tag));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ArticleCreate);
