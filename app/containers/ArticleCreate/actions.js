export const getStartLoding = () => 'ARTICLE_CREATE_PAGE/START_LOADING';
export const getStopLoding = () => 'ARTICLE_CREATE_PAGE/STOP_LOADING';
export const getHasError = () => 'ARTICLE_CREATE_PAGE/HAS_ERROR';
export const getChangeTitle = () => 'ARTICLE_CREATE_PAGE/CHANGE_TITLE';
export const getChangeDescription = () =>
  'ARTICLE_CREATE_PAGE/CHANGE_DESCRIPTION';
export const getChangeBody = () => 'ARTICLE_CREATE_PAGE/CHANGE_BODY';
export const getSubmitForm = () => 'ARTICLE_CREATE_PAGE/SUBMIT_FORM';
export const getSaved = () => 'ARTICLE_CREATE_PAGE/SAVED';
export const getSelectTag = () => 'ARTICLE_CREATE_PAGE/SELECT_TAG';
export const getUnSelectTag = () => 'ARTICLE_CREATE_PAGE/UN_SELECT_TAG';

export const selectTag = tag => ({ type: getSelectTag(), tag });
export const unSelectTag = tag => ({ type: getUnSelectTag(), tag });
export const saved = () => ({ type: getSaved() });
export const submitForm = data => ({ type: getSubmitForm(), data });
export const startLoading = () => ({ type: getStartLoding() });
export const stopLoading = () => ({ type: getStopLoding() });
export const hasError = () => ({ type: getHasError() });
export const changeTitle = title => ({
  type: getChangeTitle(),
  title,
});
export const changeDescription = description => ({
  type: getChangeDescription(),
  description,
});
export const changeBody = body => ({
  type: getChangeBody(),
  body,
});
