import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';
import { userLoggedOut } from '../Dashboard/actions';

export function Logout({ logout }) {
  useEffect(() => {
    logout();
  }, []);
  return <Redirect to="/login" />;
}

Logout.propTypes = {
  logout: PropTypes.func,
};
export function mapDispatchToProps(dispatch) {
  return {
    logout: () => {
      dispatch(userLoggedOut());
    },
  };
}

const withConnect = connect(
  undefined,
  mapDispatchToProps,
);

export default compose(withConnect)(Logout);
