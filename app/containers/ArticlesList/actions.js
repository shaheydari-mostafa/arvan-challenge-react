export const getStartGetArticles = () => 'ARTICLELIST_PAGE/GET';
export const getStartLoding = () => 'ARTICLELIST_PAGE/START_LOADING';
export const getStopLoding = () => 'ARTICLELIST_PAGE/STOP_LOADING';
export const getHasError = () => 'ARTICLELIST_PAGE/HAS_ERROR';
export const getArticlesFetched = () => 'ARTICLELIST_PAGE/ARTICLES_FETCHED';
export const getDeleteArticleCall = () => 'ARTICLELIST_PAGE/DELETE_ARTICLE';

export const deleteArticleCall = (slug, page) => ({
  type: getDeleteArticleCall(),
  slug,
  page,
});
export const startGetArticles = page => ({ type: getStartGetArticles(), page });
export const articlesFetched = (list, page, total) => ({
  type: getArticlesFetched(),
  page,
  list,
  total,
});
export const startLoading = () => ({ type: getStartLoding() });
export const stopLoading = () => ({ type: getStopLoding() });
export const hasError = () => ({ type: getHasError() });
