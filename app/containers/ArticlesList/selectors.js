import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectArticles = state => state.articles || initialState;

const makeSelectLoading = () =>
  createSelector(
    selectArticles,
    articlesState => articlesState.isLoading,
  );

const makeSelectError = () =>
  createSelector(
    selectArticles,
    articlesState => articlesState.hasError,
  );
const makeSelectList = () =>
  createSelector(
    selectArticles,
    articlesState => articlesState.list,
  );
const makeSelectTotal = () =>
  createSelector(
    selectArticles,
    articlesState => articlesState.total,
  );
const makeSelectPage = () =>
  createSelector(
    selectArticles,
    articlesState => articlesState.currentPage,
  );
export {
  makeSelectLoading,
  makeSelectError,
  makeSelectList,
  selectArticles,
  makeSelectTotal,
  makeSelectPage,
};
