import React from 'react';
import { render } from 'react-testing-library';
import { Provider } from 'react-redux';
import { browserHistory, BrowserRouter } from 'react-router-dom';

import { ArticlesList } from '../index';
import configureStore from '../../../configureStore';

describe('<ArticlesList />', () => {
  let store;

  beforeAll(() => {
    store = configureStore({}, browserHistory);
  });

  it('should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <Provider store={store}>
        <BrowserRouter>
          <ArticlesList list={[]} getList={() => {}} match={{ params: {} }} />
        </BrowserRouter>
      </Provider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
  it('should call getList with 1 as arg after mount', () => {
    const getList = jest.fn();
    render(
      <Provider store={store}>
        <BrowserRouter>
          <ArticlesList getList={getList} match={{ params: {} }} list={[]} />
        </BrowserRouter>
      </Provider>,
    );
    expect(getList).toHaveBeenCalled();
    expect(getList).toHaveBeenCalledWith(1);
  });
});
