import React, { useEffect, memo, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
} from 'reactstrap';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';

import DashboardPage from 'components/DashboardPage';
import TableBody from 'components/TableBody';
import CustomPagination from 'components/CustomPagination';
import reducer from './reducer';
import saga from './saga';
import {
  makeSelectLoading,
  makeSelectList,
  makeSelectError,
  makeSelectPage,
  makeSelectTotal,
} from './selectors';
import { startGetArticles, deleteArticleCall } from './actions';

const key = 'articles';
export function ArticlesList(props) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  // eslint-disable-next-line react/prop-types
  const currentPage = props.match.params.page || 1;
  useEffect(() => {
    props.getList(currentPage);
  }, [currentPage]);

  const [modal, setModal] = useState(false);

  const toggle = slug => {
    if (typeof slug === 'string') {
      setModal(slug);
    } else {
      setModal(!modal);
    }
  };
  const deleteArticle = () => {
    props.deleteArticleService(modal, currentPage);
    toggle();
  };

  return (
    <DashboardPage pageTitle="All Posts">
      <React.Fragment>
        <Modal isOpen={modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>Delete Article</ModalHeader>
          <ModalBody>Are you sure to delete Article?</ModalBody>
          <ModalFooter>
            <Button color="info" onClick={toggle}>
              No
            </Button>{' '}
            <Button color="danger" onClick={deleteArticle}>
              Yes
            </Button>
          </ModalFooter>
        </Modal>
        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Author</th>
              <th>Tags</th>
              <th>Excerpt</th>
              <th>Created</th>
              <th />
            </tr>
          </thead>
          <TableBody articles={props.list} onDelete={toggle} />
        </Table>
        <div style={{ textAlign: 'center' }}>
          <CustomPagination total={props.total} current={props.page} />
        </div>
      </React.Fragment>
    </DashboardPage>
  );
}
ArticlesList.propTypes = {
  getList: PropTypes.func,
  total: PropTypes.number,
  page: PropTypes.oneOf([PropTypes.number, PropTypes.string]),
  list: PropTypes.array,
  deleteArticleService: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  error: makeSelectError(),
  loading: makeSelectLoading(),
  list: makeSelectList(),
  page: makeSelectPage(),
  total: makeSelectTotal(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getList: page => {
      dispatch(startGetArticles(page));
    },
    deleteArticleService: (slug, page) => {
      dispatch(deleteArticleCall(slug, page));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(ArticlesList);
