import { takeLatest, put, call } from 'redux-saga/effects';
import request from 'utils/request';
import { BASE_URL } from 'containers/App/constants';
import { showNotification } from 'containers/Notification/actions';

import {
  getStartGetArticles,
  startLoading,
  articlesFetched,
  getDeleteArticleCall,
  startGetArticles,
} from './actions';

const GET_ARTICLES_URL = `${BASE_URL}/api/articles`;
const LIMIT = 10;

export function* getArticles(action) {
  const { page } = action;
  const offset = (page - 1) * LIMIT;
  const url = `${GET_ARTICLES_URL}?limit=${LIMIT}&offset=${offset}`;
  try {
    yield put(startLoading());
    const result = yield call(request, url);
    yield put(articlesFetched(result.articles, page, result.articlesCount));
    // eslint-disable-next-line no-empty
  } catch (err) {}
}
const DELETE_ARTICLE_URL = `${BASE_URL}/api/articles`;
export function* deleteArticle(action) {
  const url = `${DELETE_ARTICLE_URL}/${action.slug}`;
  try {
    // eslint-disable-next-line no-unused-vars
    const result = yield call(request, url, {
      method: 'DELETE',
    });
    yield put(showNotification('success', 'Article deleted successfuly'));
    yield put(startGetArticles(action.page));
  } catch (err) {
    yield put(showNotification('danger', err));
  }
}
export default function* articleListApis() {
  yield takeLatest(getStartGetArticles(), getArticles);
  yield takeLatest(getDeleteArticleCall(), deleteArticle);
}
