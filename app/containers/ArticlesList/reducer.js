import { getArticlesFetched, getStartLoding } from './actions';

export const initialState = { list: [], isLoading: false, hasError: false };

const articleListReducer = (state = initialState, action) => {
  switch (action.type) {
    case getArticlesFetched():
      return {
        ...state,
        list: action.list,
        currentPage: action.page,
        total: action.total,
      };
    case getStartLoding():
      return { ...state, isLoading: true };
    default:
      return state;
  }
};

export default articleListReducer;
