import {
  getChangeBody,
  getChangeTitle,
  getChangeDescription,
  getSaved,
  getStartLoding,
  getStopLoding,
  getArticleFetched,
  getSelectTag,
  getUnSelectTag,
} from './actions';

export const initialState = {
  isLoading: false,
  article: {
    title: '',
    description: '',
    body: '',
  },
  selectedTags: [],
};

const articleEditReducer = (state = initialState, action) => {
  switch (action.type) {
    case getChangeBody():
      return {
        ...state,
        article: {
          ...state.article,
          body: action.body,
        },
      };
    case getChangeTitle():
      return {
        ...state,
        article: {
          ...state.article,
          title: action.title,
        },
      };
    case getChangeDescription():
      return {
        ...state,
        article: {
          ...state.article,
          description: action.description,
        },
      };
    case getStartLoding():
      return { ...state, isLoading: true };
    case getStopLoding():
      return { ...state, isLoading: false };
    case getArticleFetched():
      return {
        ...state,
        article: {
          body: action.article.body,
          description: action.article.description,
          title: action.article.title,
        },
        selectedTags: action.article.tagList,
      };
    case getSelectTag():
      return { ...state, selectedTags: [...state.selectedTags, action.tag] };
    case getUnSelectTag():
      return {
        ...state,
        selectedTags: state.selectedTags.filter(tag => tag !== action.tag),
      };
    case getSaved():
      return initialState;
    default:
      return state;
  }
};

export default articleEditReducer;
