import { takeLatest, put, call, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import { get } from 'containers/Dashboard/saga';
import { getGetTags } from 'containers/Dashboard/actions';
import { BASE_URL } from 'containers/App/constants';
import { showNotification } from 'containers/Notification/actions';
import request from 'utils/request';
import sort from 'utils/sort';
import {
  getSubmitForm,
  startLoading,
  stopLoading,
  hasError,
  saved,
  articleFetched,
  getArticleGet,
} from './actions';
import { makeSelectArticle, makeSelectSelectedTags } from './selectors';

const ARTICLE_EDIT_URL = `${BASE_URL}/api/articles`;
export function* edit(action) {
  const article = yield select(makeSelectArticle());
  article.tagList = yield select(makeSelectSelectedTags());
  const body = JSON.stringify({
    article: {
      body: article.body,
      description: article.description,
      title: article.title,
      tagList: sort(article.tagList),
    },
  });
  yield put(startLoading());
  try {
    // eslint-disable-next-line no-unused-vars
    const result = yield call(request, `${ARTICLE_EDIT_URL}/${action.slug}`, {
      body,
      method: 'PUT',
    });
    yield put(
      showNotification('success', 'Well done! Article updated successfuly'),
    );
    yield put(stopLoading());
    yield put(saved());
    yield put(push('/articles'));
  } catch (err) {
    yield put(stopLoading());
    yield put(hasError());
  }
}

const ARTICLE_GET_URL = `${BASE_URL}/api/articles`;
export function* getArticleApi(action) {
  const url = `${ARTICLE_GET_URL}/${action.slug}`;
  yield put(startLoading());
  try {
    const result = yield call(request, url);
    yield put(stopLoading());
    yield put(articleFetched(result.article));
  } catch (err) {
    yield put(stopLoading());
    yield put(showNotification('danger', err));
    yield put(hasError());
  }
}

export default function* createApis() {
  yield takeLatest(getSubmitForm(), edit);
  yield takeLatest(getArticleGet(), getArticleApi);
  yield takeLatest(getGetTags(), get);
}
