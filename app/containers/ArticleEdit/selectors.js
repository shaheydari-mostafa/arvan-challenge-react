import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectArticleEdit = state => state.article_edit || initialState;

const makeSelectArticle = () =>
  createSelector(
    selectArticleEdit,
    articleSate => articleSate.article,
  );

const makeSelectLoading = () =>
  createSelector(
    selectArticleEdit,
    articleState => articleState.isLoading,
  );

const makeSelectError = () =>
  createSelector(
    selectArticleEdit,
    articleState => articleState.hasError,
  );
const makeSelectSelectedTags = () =>
  createSelector(
    selectArticleEdit,
    articleState => articleState.selectedTags,
  );
export {
  makeSelectArticle,
  selectArticleEdit,
  makeSelectLoading,
  makeSelectError,
  makeSelectSelectedTags,
};
