import React from 'react';
import { render } from 'react-testing-library';
import { Provider } from 'react-redux';
import { browserHistory, BrowserRouter } from 'react-router-dom';

import { ArticleEdit, mapDispatchToProps } from '../index';
import configureStore from '../../../configureStore';
import {
  submitForm,
  changeBody,
  changeDescription,
  changeTitle,
} from '../actions';

describe('<ArticleEdit />', () => {
  let store;

  beforeAll(() => {
    store = configureStore({}, browserHistory);
  });

  it('should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <Provider store={store}>
        <BrowserRouter>
          <ArticleEdit
            match={{ params: { slug: 'foo' } }}
            article={{}}
            getTagsService={() => {}}
            getArticle={() => {}}
          />
        </BrowserRouter>
      </Provider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
  it('should call getTagsService after mount', () => {
    const getTagsService = jest.fn();
    render(
      <Provider store={store}>
        <BrowserRouter>
          <ArticleEdit
            match={{ params: { slug: 'foo' } }}
            article={{}}
            getTagsService={getTagsService}
            getArticle={() => {}}
          />
        </BrowserRouter>
      </Provider>,
    );
    expect(getTagsService).toHaveBeenCalled();
  });

  describe('mapDispatchToProps', () => {
    describe('onSubmitForm', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.onSubmitForm).toBeDefined();
      });

      it('should dispatch submitForm when called', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.onSubmitForm();
        expect(dispatch).toHaveBeenCalledWith(submitForm());
      });
    });
    describe('handleInput', () => {
      it('should be injected', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        expect(result.handleInput).toBeDefined();
      });

      it('should dispatch changeBody when body changed', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.handleInput({ target: { name: 'body', value: 'foo' } });
        expect(dispatch).toHaveBeenCalledWith(changeBody('foo'));
      });
      it('should dispatch changeDescription when description changed', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.handleInput({ target: { name: 'description', value: 'foo' } });
        expect(dispatch).toHaveBeenCalledWith(changeDescription('foo'));
      });
      it('should dispatch changeTitle when title changed', () => {
        const dispatch = jest.fn();
        const result = mapDispatchToProps(dispatch);
        result.handleInput({ target: { name: 'title', value: 'foo' } });
        expect(dispatch).toHaveBeenCalledWith(changeTitle('foo'));
      });
    });
  });
});
