import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectNotification = state => state.notification || initialState;

export const makeSelectShow = () =>
  createSelector(
    selectNotification,
    notifState => notifState.show,
  );

export const makeSelectText = () =>
  createSelector(
    selectNotification,
    notifState => notifState.text,
  );

export const makeSelectType = () =>
  createSelector(
    selectNotification,
    notifState => notifState.type,
  );
