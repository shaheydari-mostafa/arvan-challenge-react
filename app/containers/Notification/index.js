import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Alert } from 'reactstrap';

import { useInjectReducer } from 'utils/injectReducer';
import reducer from './reducer';
import { makeSelectShow, makeSelectType, makeSelectText } from './selectors';
import { hideNotification } from './actions';

const CustomAlert = styled(Alert)`
  position: absolute;
  right: 20px;
  top: 65px;
`;

const key = 'notification';
export function Notification({ text, type, show, onDismiss }) {
  useInjectReducer({ key, reducer });

  useEffect(() => {
    if (show) {
      setTimeout(() => {
        onDismiss();
      }, 6000);
    }
  }, [show]);

  return (
    <CustomAlert isOpen={show} color={type} toggle={onDismiss}>
      {text}
    </CustomAlert>
  );
}

Notification.propTypes = {
  onDismiss: PropTypes.func,
  text: PropTypes.string,
  type: PropTypes.string,
  show: PropTypes.bool,
};
const mapStateToProps = createStructuredSelector({
  show: makeSelectShow(),
  type: makeSelectType(),
  text: makeSelectText(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onDismiss: () => {
      dispatch(hideNotification());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Notification);
