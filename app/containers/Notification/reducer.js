import { getShowNotification, getHideNotification } from './actions';

export const initialState = {
  show: false,
};

const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case getShowNotification():
      return {
        ...state,
        show: true,
        text: action.payload.text,
        type: action.payload.type,
      };
    case getHideNotification():
      return initialState;
    default:
      return state;
  }
};

export default notificationReducer;
