/* eslint-disable indent */
export const getShowNotification = () => 'NOTIFICATION/SHOW';
export const getHideNotification = () => 'NOTIFICATION/HIDE';

export const hideNotification = () => ({ type: getHideNotification() });
export const showNotification = (type, text) => ({
  type: getShowNotification(),
  payload: {
    type,
    text: getTextFromObject(text),
  },
});
const getTextFromObject = text =>
  typeof text === 'object'
    ? Object.entries(text.response.errors)
        .map(error => error.join(' '))
        .join('.')
    : text;
