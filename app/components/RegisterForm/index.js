import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import CenterColumn from 'components/CenterColumn';
import { Row, Form } from 'reactstrap';
import FormFooter from '../FormFooter';

const H1 = styled.h1`
  color: #777777;
  text-align: center;
`;
export default function RegisterForm(props) {
  return (
    <Row className="h-100 align-items-center">
      <CenterColumn xs="12" sm="12" md="6" lg="4" xl="3">
        <H1>Register</H1>
        <Form onSubmit={props.onSubmitForm}>
          {props.children}
          <FormFooter
            textValue="Already Registered?"
            anchorValue="Login"
            anchorLink="/login"
          />
        </Form>
      </CenterColumn>
    </Row>
  );
}

RegisterForm.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element.isRequired),
  onSubmitForm: PropTypes.func,
};
