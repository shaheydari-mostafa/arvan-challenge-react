import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Row, Nav, NavItem } from 'reactstrap';

const LinkObject = styled(Link)`
  color: #ffffff;
  margin-left: 20px;
`;
const NavTitle = styled.p`
  margin-left: 10px;
  color: #ffffff;
  font-size: 2rem;
`;
const NavItemWithHover = styled(NavItem)`
  padding: 10px 0 10px 0;
  :hover {
    background-color: rgba(255, 255, 255, 0.1);
  }
`;
export default function leftSideNav() {
  return (
    <Row>
      <div className="fullWidth">
        <NavTitle>Post</NavTitle>
        <Nav vertical>
          <NavItemWithHover>
            <LinkObject to="/">All Articles</LinkObject>
          </NavItemWithHover>
          <NavItemWithHover>
            <LinkObject to="/articles/create">New Article</LinkObject>
          </NavItemWithHover>
        </Nav>
      </div>
    </Row>
  );
}
