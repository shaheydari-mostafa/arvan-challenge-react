import React, { useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FormGroup, Input, Label, Form } from 'reactstrap';

import CustomInput from 'components/CustomInput';
import sort from 'utils/sort';

const TagsHolder = styled.div`
  padding: 15px;
  border: 1px solid #ced4da;
  border-radius: 0.25rem;
`;
export default function TagSection({ tags, selectedTags, onSelectTag }) {
  const [newTag, setNewTag] = useState('');

  if (!tags) {
    return null;
  }
  const changeNewTag = ev => {
    setNewTag(ev.target.value);
  };
  const sortedTags = sort(tags);
  const tagsList = sortedTags.map(tag => (
    <FormGroup check>
      <Label check>
        <Input
          onChange={ev => onSelectTag(tag, ev.target.checked)}
          type="checkbox"
          checked={selectedTags && selectedTags.includes(tag)}
        />{' '}
        {tag}
      </Label>
    </FormGroup>
  ));
  return (
    <React.Fragment>
      <Form
        onSubmit={ev => {
          ev.preventDefault();
          onSelectTag(newTag, true, true);
          setNewTag('');
        }}
      >
        <CustomInput
          name="tag"
          type="text"
          id="tagField"
          label="Tags"
          valid="true"
          value={newTag}
          placeholder="New Tag"
          onChange={changeNewTag}
        />
      </Form>
      <TagsHolder>{tagsList}</TagsHolder>
    </React.Fragment>
  );
}

TagSection.propTypes = {
  tags: PropTypes.array,
  selectedTags: PropTypes.array,
  onSelectTag: PropTypes.func,
};
