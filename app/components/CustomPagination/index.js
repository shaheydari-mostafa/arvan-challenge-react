import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

export default function customPagination({ total, current }) {
  const links = [];
  const totalPage = Math.ceil(total / 10);
  // eslint-disable-next-line no-plusplus
  for (let i = current, j = 0; i < totalPage && j < 4; i++, j++) {
    links.push(
      <Link to={i === 1 ? '/articles' : `/articles/page/${i}`}>
        <PaginationLink key={i}>{i}</PaginationLink>
      </Link>,
    );
  }
  const hasPrevious = current > 1;
  const hasNext = current < total;
  return (
    <div style={{ display: 'inline-block' }}>
      <Pagination>
        {hasPrevious ? (
          <PaginationItem>
            <Link
              to={
                current - 1 === 1
                  ? '/articles'
                  : `/articles/page/${current - 1}`
              }
            >
              <PaginationLink previous />
            </Link>
          </PaginationItem>
        ) : null}
        {links}
        {hasNext ? (
          <PaginationItem>
            <Link to={`/articles/page/${current + 1}`}>
              <PaginationLink next />
            </Link>
          </PaginationItem>
        ) : null}
      </Pagination>
    </div>
  );
}
customPagination.propTypes = {
  total: PropTypes.number,
  current: PropTypes.number,
};
