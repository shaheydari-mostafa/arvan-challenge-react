import React from 'react';
import PropTypes from 'prop-types';
import { Input, FormGroup, Label, FormFeedback } from 'reactstrap';

export default function customInput({
  valid,
  value,
  onChange,
  name,
  type,
  id,
  label,
  placeholder,
}) {
  return (
    <FormGroup>
      <Label className={!valid ? 'text-danger' : ''} for={id}>
        {label}
      </Label>
      <Input
        type={type}
        name={name}
        id={id}
        value={value}
        invalid={!valid}
        onChange={onChange}
        placeholder={placeholder}
      />
      <FormFeedback invalid>Required field</FormFeedback>
    </FormGroup>
  );
}

customInput.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string.isRequired,
  id: PropTypes.string,
  value: PropTypes.any,
  valid: PropTypes.bool,
  onChange: PropTypes.func,
  label: PropTypes.string,
  placeholder: PropTypes.string,
};
customInput.defaultProps = {
  valid: true,
};
