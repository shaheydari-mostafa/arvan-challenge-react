import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styled from 'styled-components';

const AnchorRegisterLink = styled(Link)`
  padding-left: 1rem;
  color: #000;
  font-weight: bold;
`;
const FooterInLogin = styled.p`
  margin-top: 10px;
`;

export default function formFooter({ textValue, anchorValue, anchorLink }) {
  return (
    <FooterInLogin>
      {textValue}
      <AnchorRegisterLink to={anchorLink}>{anchorValue}</AnchorRegisterLink>
    </FooterInLogin>
  );
}

formFooter.propTypes = {
  textValue: PropTypes.string,
  anchorValue: PropTypes.string,
  anchorLink: PropTypes.string,
};
