import { Col } from 'reactstrap';
import styled from 'styled-components';

const CenterColumn = styled(Col)`
  @media (min-width: 768px) {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;
export default CenterColumn;
