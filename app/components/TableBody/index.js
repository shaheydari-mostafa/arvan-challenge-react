import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
} from 'reactstrap';
import PropTypes from 'prop-types';

import formateDate from 'utils/formateDate';

export default function TableBody({ articles, onDelete }) {
  const [opens, setOpens] = useState(new Array(10).fill(false));
  const onClicks = i => {
    const changed = opens.map((item, index) => {
      if (index === i) {
        return !item;
      }
      return item;
    });
    setOpens(changed);
  };
  const rows = articles.map((article, i) => (
    <tr key={article.slug}>
      <th scope="row">{i + 1}</th>
      <td>{article.title}</td>
      <td>@{article.author.username}</td>
      <td>{article.tagList.join(', ')}</td>
      <td>{article.description}</td>
      <td>{formateDate(article.createdAt)}</td>
      <td>
        <ButtonDropdown isOpen={opens[i]} toggle={ev => onClicks(i, ev)}>
          <DropdownToggle caret color="info">
            ...
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem>
              <Link to={`/articles/edit/${article.slug}`}>Edit</Link>
            </DropdownItem>
            <DropdownItem onClick={() => onDelete(article.slug)}>
              Delete
            </DropdownItem>
          </DropdownMenu>
        </ButtonDropdown>
      </td>
    </tr>
  ));
  return <tbody>{rows}</tbody>;
}

TableBody.propTypes = {
  articles: PropTypes.array,
  onDelete: PropTypes.func,
};
