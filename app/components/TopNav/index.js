import React, { useState } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Button,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavLink,
  NavItem,
} from 'reactstrap';

const UserLi = styled.span`
  font-size: 1.4rem;
  color: #ffffff;
`;
const ButtonWrapper = styled(Button)`
  :hover {
    text-decoration: none !important;
    background-color: transparent !important;
  }
`;
const LinkWrapper = styled(Link)`
  text-decoration: none !important;
  color: ${props => props.color};
  :hover {
    text-decoration: none !important;
  }
`;
export default function TopNav({ username }) {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => {
    setIsOpen(prev => !prev);
  };
  return (
    <Navbar color="dark" dark expand="md">
      <NavbarBrand style={{ color: '#ffffff' }}>Arvan Challenge</NavbarBrand>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar>
        <Nav navbar>
          <UserLi>Welcome {username}</UserLi>
        </Nav>
        <Nav className="ml-auto" navbar>
          <NavItem className="d-md-none">
            <NavLink>
              <LinkWrapper color="#ffffff" to="/articles">
                All Articles
              </LinkWrapper>
            </NavLink>
          </NavItem>
          <NavItem className="d-md-none">
            <NavLink>
              <LinkWrapper color="#ffffff" to="/articles/create">
                New Article
              </LinkWrapper>
            </NavLink>
          </NavItem>
          <NavItem>
            <ButtonWrapper color="info" outline>
              <LinkWrapper color="#17a2b8" to="/logout">
                Logout
              </LinkWrapper>
            </ButtonWrapper>
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
  );
}
TopNav.propTypes = {
  username: PropTypes.string,
};
