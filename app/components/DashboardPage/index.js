import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const PageTitle = styled.h1`
  margin-top: 30px;
  margin-bottom: 30px;
`;
export default function dashboardPage({ pageTitle, children }) {
  return (
    <div>
      <PageTitle>{pageTitle}</PageTitle>
      {children}
    </div>
  );
}

dashboardPage.propTypes = {
  pageTitle: PropTypes.string,
  children: PropTypes.element,
};
