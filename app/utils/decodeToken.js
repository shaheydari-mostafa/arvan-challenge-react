const decodeToken = token => ({
  ...JSON.parse(atob(token.split('.')[1])),
  token,
});

export default decodeToken;
