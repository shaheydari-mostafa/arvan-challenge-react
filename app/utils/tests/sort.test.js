import sort from '../sort';
describe('sort tests', () => {
  it('should return array', () => {
    const sorted = sort([]);
    expect(Array.isArray(sorted)).toBe(true);
  });
  it('should return sorted array', () => {
    const sorted = sort(['b', 'a', 'ab']);
    expect(sorted).toEqual(['a', 'ab', 'b']);
  });
});
