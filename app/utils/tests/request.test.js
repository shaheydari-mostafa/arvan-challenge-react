import 'whatwg-fetch';
import request from '../request';

describe('request tests', () => {
  beforeEach(() => {
    window.fetch = jest.fn();
  });
  describe('ok response', () => {
    beforeEach(() => {
      window.fetch.mockReturnValue(
        Promise.resolve(
          new Response('{"challenge": "valid"}', {
            status: 200,
            headers: {
              'Content-Type': 'application/json; charset=utf-8',
            },
          }),
        ),
      );
    });

    it('should return response', done => {
      request('/challenge')
        .then(res => {
          expect(res.challenge).toBe('valid');
          done();
        })
        .catch(done);
    });
  });
  describe('error response', () => {
    beforeEach(() => {
      window.fetch.mockReturnValue(
        Promise.resolve(
          new Response('{"errors": {"challenge": "invalid"}}', {
            status: 404,
            headers: {
              'Content-Type': 'application/json; charset=utf-8',
            },
          }),
        ),
      );
    });

    it('should catch', done => {
      request('/challenge').catch(error => {
        expect(error.response.errors).toEqual({ challenge: 'invalid' });
        done();
      });
    });
  });
});
