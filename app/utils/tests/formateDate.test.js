import formateDate from '../formateDate';

describe('formate date tests', () => {
  it('should return string', () => {
    const result = formateDate(new Date());
    expect(typeof result).toBe('string');
  });

  it('should return valid string', () => {
    const result = formateDate('2019-11-13T05:07:46.215Z');
    expect(result).toBe('November 13 ,2019');
  });
});
