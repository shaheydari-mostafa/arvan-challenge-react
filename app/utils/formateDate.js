const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

/**
 * @param {string} dateStr
 * @returns {string}
 */
export default function formateDate(dateStr) {
  const date = new Date(dateStr);
  const day = date.getDate();
  const indexOfMonth = date.getMonth();
  const year = date.getFullYear();

  return `${monthNames[indexOfMonth]} ${day} ,${year}`;
}
