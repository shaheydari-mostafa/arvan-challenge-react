export const defaultHeaders = {
  'Content-Type': 'application/json; charset=utf-8',
};
/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  if (response.status === 204 || response.status === 205) {
    return null;
  }
  return response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  return parseJSON(response).then(result => {
    error.response = result;
    throw error;
  });
}

/**
 *
 * @param {object} options passed options
 */
function customHeaders(options) {
  if (!options) {
    return {
      headers: defaultHeaders,
    };
  }
  if (options.headers) {
    return {
      ...options,
      headers: {
        ...options.headers,
        ...defaultHeaders,
      },
    };
  }
  return {
    ...options,
    headers: defaultHeaders,
  };
}
/**
 *
 * @param {string} url Request url
 * @param {object} options options for fetch API
 */
export default function request(url, options) {
  return fetch(url, customHeaders(options))
    .then(checkStatus)
    .then(parseJSON);
}
